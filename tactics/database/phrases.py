"""
Handles a documents collection in the TacticsDB

This class should subclass a MongoDB collection object.
"""

from tactics import static
from tactics.errors import ReplyException

import pymongo
from pymongo.collection import Collection
from pymongo.errors import OperationFailure, DuplicateKeyError

from datetime import datetime

import logging

"""	Phrases database """
class Phrases (Collection):
	def __init__ (self, database, config):
		# Init the pymongo object
		Collection.__init__(self, database,
			config.get('db_phrases_collection','phrases_collection'))

		# Create logger
		self.logger = logging.getLogger(__name__)

		# Ensures the collection of documents is indexed by _id and subject
		self.logger.info("Ensuring indexes exist...")
		self.ensure_index('_id', unique = True, drop_dups = True)
		self.ensure_index('lc_subject')

		# Check phrase count
		try:
			count = self.count()
		except TypeError:
			# Count is not set, set it to a sane value
			self.database.info.save({'_id': self.counter_name, 'count': self.find_highest_id()})
		finally:
			self.logger.info("Collection {} loaded - {}: {}".format(
				self.full_name, self.counter_name, self.count()))

		# Initialise variables
		self.last = None

	"""	Input """

	def create (self, phrase, link, metadata = {}):
		""" Create a phrase and add it to the collection """
		try:
			# Check that data has all needed keys
			if not 'subject' in phrase:
				raise ReplyException("Phrase has no subject!")
			if not 'text' in phrase:
				raise ReplyException("Phrase has no text!")

			# Check that a valid link type has been used
			if not link in static.links:
				raise ReplyException("Link type not valid")

			# Create initial phrase document
			phrase['link'] = link
			phrase['text'] = self.prepare(phrase['text'])

			# Check that phrase does not already exist
			if self.find(phrase).count() > 0:
				raise ReplyException("Phrase already exists in {}".format(self.full_name))

			# Set metadata, including time of creation
			phrase['meta'] = metadata
			phrase['meta']['time-created'] = datetime.now()

			# Set index
			phrase['lc_subject'] = phrase['subject'].lower()

			# Get a phrase _id
			phrase['_id'] = self.new_count()

			# Finally, insert the phrase
			self.insert(phrase, safe=True)
		except OperationFailure as e:
			# Database could not add phrase
			raise ReplyException("Could not add phrase to database.")
		except DuplicateKeyError as e:
			# Phrase key already exists
			raise ReplyException("A phrase with this id is alread in the database - the phrase count may be incorrect.")
		else:
			# Return the phrase _id
			return self.find_one(phrase)['_id']

	"""	Phrase counter """

	counter_name = property(lambda self: "{}_count".format(self.name))

	def count (self):
		"""	Get the current phrase count """
		return int(self.database.info.find_one({'_id': self.counter_name})['count'])

	def set_count (self, c):
		"""	Modify the current phrase count """
		self.database.info.update({'_id': self.counter_name},{'$set': {'count': c}})

	def inc_count (self, c):
		self.database.info.update({'_id': self.counter_name},{'$inc': {'count': c}})

	def new_count (self):
		"""	Increment the current count, then return it """
		self.inc_count(1)
		return self.count()

	def find_highest_id (self):
		"""	Try and find the highest _id in the phrases collection """
		highest_id = self.find_one({}, fields=['_id'], sort=[('_id', pymongo.DESCENDING)])
		return highest_id['_id'] if highest_id else 0

	""" Searching """

	def get_phrases_by_subject (self, subject):
		"""	Search collection for phrases with given subject """
		return self.find({'lc_subject': subject.lower()})

	def get_phrase_by_id (self, _id):
		"""	Get a phrase from the collection by it's id """
		self.logger.debug("Getting phrase #{:>06}".format(_id))
		phrase = self.find_one({'_id': int(_id)})
		if not phrase:
			raise ReplyException("Phrase #{:>06} does not exist.".format(_id))
		else:
			return phrase

	"""	Output """

	def format (self, phrase_document):
		"""	Return a phrase formated with it's link format """
		link_format = static.links[phrase_document['link']][1]
		formated_phrase = link_format % phrase_document
		return formated_phrase

	def prepare (self, text):
		"""	Prepare the text of a phrase for placing into the database """
		return text.strip().strip('.')

	"""	Deletion """

	def delete (self, _id):
		"""	Delete a phrase from the collection """
		self.remove(self.get_phrase_by_id(_id))
		self.logger.info("Phrase #{:>06} deleted.".format(_id))
		if self.count() == _id:
			self.inc_count(-1)
			self.logger.debug("Set count to {}".format(self.count()))


"""	Error messages """
class NoPhraseFound (Exception): pass
