"""
Static data for Tactics
"""
objectid	= '[abcdef\d]{24}'

# Link types
links = {
	# <subject> <verb> <text>
	'is':		("^(?P<subject>[\w ]+) is (?P<text>.+)", "%(subject)s is %(text)s"),
	'are':		("^(?P<subject>[\w ]+) are (?P<text>.+)", "%(subject)s are %(text)s"),
	'likes':	("^(?P<subject>[\w ]+) likes (?P<text>.+)", "%(subject)s likes %(text)s"),
	'verb':		("^(?P<subject>[\w ]+) \*(?P<verb>.+)\* (?P<text>.+)", "%(subject)s %(verb)s %(text)s"),

	# <subject>'s <text>
	'owns':		("^(?P<subject>[\w ]+) owns (?P<text>.+)", "%(subject)s's %(text)s"),

	# <text>
	'reply':	("^(?P<subject>[\w ]+) <reply> (?P<text>.+)", "%(text)s"),
	'action':	("^(?P<subject>[\w ]+) <action> (?P<text>.+)", "\001ACTION%(text)s\001"),
	'special':	("^(?P<subject>[\w ]+) ! (?P<text>.+)", "%(text)s")
}
help_links = """Valid link types are 'is', 'are', '*{verb}*', 'owns', '<reply>', '<action>', '!'."""

# Genders and pronouns
pronouns = ("subjective", "objective", "reflexive", "possessive", "determiner")
genders = {
	   "male": ("he", "him", "himself", "his", "his"),
	 "female": ("she", "her", "herself", "hers", "her"),
"androgynous": ("they", "them", "themself", "theirs", "their"),
 "non-binary": ("zie", "hir", "hirself", "hirs", "hir"),
  "inanimate": ("it", "it", "itself", "its", "its"),
	   "name": ("{name}", "{name}", "{name}", "{name}'s", "{name}'s")
}
help_genders = """"""

# Place all of the gender values into dicts
for g, gender in genders.items():
	genders[g] = dict(zip(pronouns, gender))

# Help message
help = """
Phrases are in the form "<subject> <link> <text>". Don't use punctuation.
{help_links}
"""
help = help.format(help_links = help_links)
