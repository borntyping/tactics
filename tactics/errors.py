"""	Exceptions """

class ReplyException (Exception):
	"""	Class for exceptions that should be handled as a reply to the user """
	pass
