"""
Dynamic commands for tactics

Tactics can reload all commands in the module by calling Tactics.reload_commands().

Regexes must use named groups for arguments to be passed to the function.
Commands that need a priority should use '(?#n)' at the start of the regex.

Commands may be a string or a function.
 - Strings will be sent as a reply to the nick that triggered the command.
 - Functions will be called and should handle their own output.

"""

from tactics import static

dynamic_commands = {
	'^help': static.help,
	'^reload': lambda self, msg: self.reload_commands() or msg.reply("Reloaded commands"),

	# Say [#<channel>] <text>
	'(?#1)^say #(?P<chan>.+) (?P<text>.+)$': lambda _, msg, chan, text: msg.server.privmsg("#"+chan, text),
	'(?#2)^say (?P<text>.+)$': lambda _, msg, text: msg.reply(text),

	# Get quote by id
	'^get #(?P<id>\d+)': lambda self, msg, id: msg.reply(self.phrases.format(self.phrases.get_phrase_by_id(id))),

	# Get id of last quote
	'^what was that': lambda self, msg: msg.reply("That was #{:>06}".format(self.phrases.last['_id']) if self.phrases.last else "I didn't say anything!"),
}
