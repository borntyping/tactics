#!/bin/sh

DBPATH=./data/
LOGFILE=mongo.log

mkdir -p $DBPATH
mongod --rest --dbpath $DBPATH --logpath ${DBPATH}${LOGFILE} --logappend --fork
