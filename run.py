#!/usr/bin/env python

from tactics.core import Tactics

tactics = Tactics({
	'nick' : 'TacticsBot',
	
	'log_verbosity': 8,
	
	'servers' : [{
		'id': 'AFNet',
		'address': 'irc.aberwiki.org',
		'channels': ['#aleph']
	}],
})

tactics.run()
